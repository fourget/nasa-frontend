package it.fourget.frontend.spotfire;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;
import org.osmdroid.views.overlay.Overlay;

import static java.lang.Math.cos;

/*
 * NASA SpaceApps Challenge 2018
 *
 * Spot that fire!
 *
 * Four-GET team (Rome)
 * Belocchi Giacomo, Leyland Emanuele, Mastropietro Flavio, Taglioni Roberto
 * Thanks to https://stackoverflow.com/questions/14672965/not-able-to-draw-a-circle-around-my-current-location-in-map
 */

public class CircleOverlay extends Overlay {

    Context context;
    double mLat;
    double mLon;
    float mRadius;

    public CircleOverlay(Context _context, double _lat, double _lon, float radius ) {
        context = _context;
        mLat = _lat;
        mLon = _lon;
        mRadius = radius;

    }

    public void draw(Canvas canvas, MapView mapView, boolean shadow) {
        if(shadow) return; // Ignore the shadow layer

        Projection projection = mapView.getProjection();

        Point pt = new Point();

        GeoPoint geo = new GeoPoint(mLat, mLon);

        projection.toPixels(geo ,pt);
        float circleRadius = (float) (projection.metersToEquatorPixels(mRadius) * (1/ cos((double) Math.toRadians(mLat))));

        Paint innerCirclePaint;

        innerCirclePaint = new Paint();
        innerCirclePaint.setColor(Color.RED);
        innerCirclePaint.setAlpha(40);
        innerCirclePaint.setAntiAlias(true);

        innerCirclePaint.setStyle(Paint.Style.FILL);

        canvas.drawCircle((float)pt.x, (float)pt.y, circleRadius, innerCirclePaint);

    }
}