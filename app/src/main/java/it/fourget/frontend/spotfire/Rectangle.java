package it.fourget.frontend.spotfire;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Rectangle {

    private Coordinate upRight;
    private Coordinate lowLeft;
}
