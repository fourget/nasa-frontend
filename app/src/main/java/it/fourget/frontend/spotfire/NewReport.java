package it.fourget.frontend.spotfire;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NewReport extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private FusedLocationProviderClient mFusedLocationClient;

    private String IMEI;
    private Coordinate coordinate;
    private Long timestamp; //in millis


    private EnvironmentType terrain;
    private EnvironmentType houses;
    private EnvironmentType population;

    private Button pictBtn3;
    private TextView precisionValue;

    private RadioButton A1;
    private RadioButton A2;
    private RadioButton A3;
    private RadioButton B1;
    private RadioButton B2;
    private RadioButton C1;
    private RadioButton C2;

    private Form form;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_report);

        form = Form.getInstance();

        pictBtn3 = findViewById(R.id.pictBtn3);

        A1 = findViewById(R.id.radioA1);
        A2 = findViewById(R.id.radioA2);
        A3 = findViewById(R.id.radioA3);
        B1 = findViewById(R.id.radioB1);
        B2 = findViewById(R.id.radioB2);
        C1 = findViewById(R.id.radioC1);
        C2 = findViewById(R.id.radioC2);

        C1.toggle();
        A1.toggle();
        B1.toggle();

        RadioGroup groupA = (RadioGroup) findViewById(R.id.groupA);
        RadioGroup groupB = (RadioGroup) findViewById(R.id.groupB);
        RadioGroup groupC = (RadioGroup) findViewById(R.id.groupC);

        if(A1.isSelected()){
            terrain = EnvironmentType.GRASS;

        }
        else if(A2.isSelected()){
            terrain = EnvironmentType.FOREST;

        }
        else if(A3.isSelected()){
            terrain = EnvironmentType.URBAN;

        }
        if(B1.isSelected()){
            houses = EnvironmentType.LOWDENSITY;

        }
        else if(B2.isSelected()){
            houses = EnvironmentType.HIGHDENSITY;

        }

        if(C1.isSelected()){
            population = EnvironmentType.NOPEOPLE;

        }
        else if(C2.isSelected()){
            population = EnvironmentType.PEOPLE;

        }

        C1.toggle();
        population = EnvironmentType.NOPEOPLE;
        A1.toggle();
        terrain = EnvironmentType.GRASS;
        B1.toggle();
        houses = EnvironmentType.LOWDENSITY;

        /*

        groupA.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
            }
        });

        groupB.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
            }
        });

        groupC.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
            }
        });

        */

        pictBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (terrain == null || population == null || houses == null) {
                    showToast("Please, make one choice for every option(environment, houses and population)" +
                            "in order to make your report more informative");
                } else {*/
                    getIMEI();
                    getLocation();
                    getMillis();
                    getInfo();
                    Intent intent = new Intent(view.getContext(), NewReport2.class);
                    intent.putExtra("prova", "123");
                    startActivity(intent);
                //}
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    }

    private void getInfo(){
        List<EnvironmentType> list = new ArrayList<EnvironmentType>();
        list.add(terrain);
        list.add(population);
        list.add(houses);
        form.getCurrentReport().setEnv(list);
    }

    private void getLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            coordinate = new Coordinate(location.getLatitude(), location.getLongitude());
                            form.getCurrentReport().setCoordinate(coordinate);
                            Log.e("latitude", Double.toString(location.getLatitude()));
                            // Logic to handle location object
                        } else {
                            Log.e("latitude", "no location");

                        }
                    }
                });

    }


    public NewReport(){

    }

    private void getIMEI(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        TelephonyManager t = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        IMEI = t.getDeviceId();
        form.getCurrentReport().setUserID(IMEI);
        Log.e("myID", t.getDeviceId());
    }
    private void getMillis(){
        timestamp = System.currentTimeMillis();
        form.getCurrentReport().setTimestamp(timestamp);
    }

    private boolean checkInput(){
        if(IMEI == null || IMEI == ""){
            showToast("error on IMEI");
            return false;
        }
        return true;

    }

    private void showToast(String msg) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();
    }

    private void report(){
        if(checkInput()){


        }
    }

}
