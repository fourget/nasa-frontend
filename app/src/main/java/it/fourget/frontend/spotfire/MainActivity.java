package it.fourget.frontend.spotfire;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.Toast;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.DelayedMapListener;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private WildfireCRUD wildfirecrud;
    private Button newReport;
    private org.osmdroid.views.MapView map = null;
    private Coordinate coordinate = new Coordinate();
    private Context ctx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        setContentView(R.layout.activity_main);

        map = findViewById(R.id.map);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        map.setTileSource(TileSourceFactory.MAPNIK);
        IMapController mapController = map.getController();
        mapController.setZoom(50);

        Location location = getLocation();
        IGeoPoint startPoint = new GeoPoint(location.getLatitude(), location.getLongitude());
        mapController.setCenter(startPoint);
        map.invalidate();

        updatePoints();

        map.setMapListener(new DelayedMapListener(new MapListener() {
            public boolean onZoom(final ZoomEvent e) {
                updatePoints();

                return true;
            }

            public boolean onScroll(final ScrollEvent e) {
                updatePoints();

                return true;
            }
        }, 100));

        map.scrollTo(0, 0);


        setContentView(R.layout.activity_main);
        newReport = findViewById(R.id.reportBtn);


        newReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), NewReport.class);
                intent.putExtra("prova", "123");
                startActivity(intent);
            }
        });
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.2.148.248:5000")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        wildfirecrud = retrofit.create(WildfireCRUD.class);

        showToast("Press the add icon if you want to report a wildfire!");

    }

    private void showToast(String msg) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();

    }

    private void updatePoints() {
        Coordinate upRight = new Coordinate(map.getProjection().getNorthEast().getLatitude(), map.getProjection().getNorthEast().getLongitude());
        Coordinate lowLeft = new Coordinate(map.getProjection().getSouthWest().getLatitude(), map.getProjection().getSouthWest().getLongitude());

        Rectangle rectangle = new Rectangle(upRight, lowLeft);

        Log.d("rectangle", rectangle.toString());

        map.getOverlays().clear();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.2.147.69:8080")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        wildfirecrud = retrofit.create(WildfireCRUD.class);
        final Call<List<NasaWildfire>> call = wildfirecrud.getNasaWildfires(rectangle);
        call.enqueue(new Callback<List<NasaWildfire>>() {
            @Override
            public void onResponse(Call<List<NasaWildfire>> call, Response<List<NasaWildfire>> response) {
                if (!response.isSuccessful())
                    Toast.makeText(ctx, getResources().getText(R.string.server_error), Toast.LENGTH_LONG).show();
                else {

                    List<NasaWildfire> fires = response.body();

                    for (NasaWildfire w: fires){
                        CircleOverlay circleOverlay = new CircleOverlay(getApplicationContext(), w.getCoordinate().getLatitude(), w.getCoordinate().getLongitude(), 1000);
                        map.getOverlays().add(circleOverlay);
                        map.invalidate();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<NasaWildfire>> call, Throwable t) {

            }
        });

        final Call<List<Wildfire>> call1 = wildfirecrud.getWildfires(rectangle);
        call1.enqueue(new Callback<List<Wildfire>>() {
            @Override
            public void onResponse(Call<List<Wildfire>> call, Response<List<Wildfire>> response) {
                if (!response.isSuccessful())
                    Toast.makeText(ctx, getResources().getText(R.string.server_error), Toast.LENGTH_LONG).show();
                else {

                    List<Wildfire> fires = response.body();

                    Drawable newMarker = getResources().getDrawable(R.drawable.ic_fireicon05);


                    for (Wildfire w: fires){
                        Marker marker = new Marker(map, ctx);
                        marker.setTitle(getResources().getString(R.string.wildfire_marker));
                        marker.setSubDescription(Double.toString(w.getCoordinate().getLatitude())+" "+Double.toString(w.getCoordinate().getLongitude()));
                        marker.setIcon(newMarker);
                        marker.setPosition(new GeoPoint(w.getCoordinate().getLatitude(), w.getCoordinate().getLongitude()));
                        map.getOverlays().add(marker);
                        map.invalidate();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Wildfire>> call, Throwable t) {

            }

        });

    }

    public Location getLocation() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(ctx, getResources().getString(R.string.permission_error),Toast.LENGTH_LONG).show();
                finish();
            }
            Location lastKnownLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (lastKnownLocationGPS != null) {
                return lastKnownLocationGPS;
            } else {
                Location loc =  locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                return loc;
            }
        } else {
            return null;
        }
    }
}
