package it.fourget.frontend.spotfire;

public enum EnvironmentType {
    GRASS, FOREST, URBAN, LOWDENSITY, HIGHDENSITY, NOPEOPLE, PEOPLE
}
