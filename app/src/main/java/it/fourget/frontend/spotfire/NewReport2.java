package it.fourget.frontend.spotfire;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static it.fourget.frontend.spotfire.NewReport.REQUEST_IMAGE_CAPTURE;

public class NewReport2 extends AppCompatActivity {

    private Double precision; //in km 0-3
    private String picture;
    private Form form;


    private Button pictBtn, pictBtn2;
    private TextView precisionValueText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_report2);

        form = Form.getInstance();

        pictBtn = findViewById(R.id.pictBtn);
        pictBtn2 = findViewById(R.id.pictBtn2);
        precisionValueText = findViewById(R.id.precisionValueText);

        pictBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });


        precision = 0.0;
        precisionValueText.setText("about " + precision + " km");
        SeekBar seek = (SeekBar) findViewById(R.id.seekBar);

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub

                precision = progress/2.0;
                precisionValueText.setText("about " + precision+ " km");

            }
        });

        pictBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPrecision();
                getPhoto();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://10.2.148.248:5000")
                        .addConverterFactory(JacksonConverterFactory.create())
                        .build();

                WildfireCRUD wildfirecrud = retrofit.create(WildfireCRUD.class);
                wildfirecrud.report(form.getCurrentReport());
                form.reset();

                showToast("Thanks for submitting this reporting. " +
                        "Now get yourself safe!");
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });


    }

    private void getPhoto() {
        form.getCurrentReport().setPicture(picture);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            String filename = "lastPicture.png";
            File sd = Environment.getExternalStorageDirectory();
            File dest = new File(sd, filename);
            try {
                FileOutputStream out = new FileOutputStream(dest);
                imageBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }


            //TODO BASE 64 FROM FILE

            File file = new File("/storage/emulated/0/lastPicture.png");
            int size = (int) file.length();
            byte[] bytes = new byte[size];
            try {
                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                buf.read(bytes, 0, bytes.length);
                buf.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String encoded = Base64.encodeToString(bytes, Base64.DEFAULT);
            Log.e("new64", encoded);

            picture = encoded;


            File deleteFile = new File("/storage/emulated/0/lastPicture.png");
            file.delete();

        }

    }

    private void getPrecision(){
        String precisionString = precisionValueText.getText().toString();
        if (precisionString != null){
            form.getCurrentReport().setPrecision(precision);
        }

    }

    private void showToast(String msg) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();
    }
}
