package it.fourget.frontend.spotfire;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface WildfireCRUD {

    @POST("wildfire")
    Call<List<Wildfire>> getWildfires(@Body Rectangle r); //TODO


    @POST("nasaWildfire")
    Call<List<NasaWildfire>> getNasaWildfires(@Body Rectangle r); //TODO



    @POST("report/new")
    Call<Wildfire> report(@Body Reporting report);


    @POST("/isFire")
    Call<String> img(@Body String s);



}
