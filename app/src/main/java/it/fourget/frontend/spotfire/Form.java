package it.fourget.frontend.spotfire;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Form {
    private static final Form ourInstance = new Form();

    public static Form getInstance() {
        return ourInstance;
    }

    private Reporting currentReport;

    private Form() {
        currentReport = new Reporting();
    }

    public void reset() {
        currentReport = new Reporting();
    }

}
