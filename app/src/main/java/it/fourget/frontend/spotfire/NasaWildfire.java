package it.fourget.frontend.spotfire;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class NasaWildfire {
    Date date;
    Coordinate coordinate;
}
