package it.fourget.frontend.spotfire;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Reporting {
    String userID; //imei
    Coordinate coordinate; //position
    Double precision; //in km 0-3
    String picture;
    Long timestamp; //in millis
    List<EnvironmentType> env;

}
